<?php
/**
 * Description of Crud
 *
 * @author Mick Hill
 */

class Crud
{
    private $host  = "localhost";
    private $login = "root";
    private $senha = "root";
    private $banco = "crud";
    private $con;


    public function __construct()
    {
        $mysqli = new mysqli($this->host, $this->login, $this->senha, $this->banco);
        
        if ($mysqli->connect_errno)
        {
            printf("Erro de conexao: %s\n", $mysqli->connect_error);
            die();
        }
        else
        {
            $this->con = $mysqli;
        }
    }
    
    
    
    public function creat( $in_table, $in_dados = array("campo_tabela"=>"dado_compo") )
    {
        $campos  = "`" . implode("`, `", array_keys($in_dados)) . "`";
        $valores = "'" . implode("', '", $in_dados) . "'";
        
        $query   = "INSERT INTO `$this->banco`.`$in_table` ($campos) VALUES ($valores)";
        $result  = $this->con->query($query);
        
        return $result;
    }
    
    
    
    public function read($in_query)
    {
        return $this->con->query($in_query);
    }
    
    
    
    public function update( $in_table, $in_id, $in_new_dados = array("campo_tabela"=>"dado_compo") )
    {
        foreach ($in_new_dados as $campo_tabela => $dado_compo)
        {
            $user_arr[] = "`$campo_tabela`='$dado_compo'";
        }
        $in_new_dados = implode(", ", $user_arr);
        
        
        $query = "UPDATE `$this->banco`.`$in_table` SET $in_new_dados WHERE `id`='$in_id';";
        $result  = $this->con->query($query);
        
        return $result;
    }
    
    
    
    public function delet($in_table, $in_id)
    {
        $query  = "DELETE FROM `$this->banco`.`$in_table` WHERE `id`='$in_id'";
        $result = $this->con->query($query);
        
        return $result;
    }
    
    
    
    public function list_table($in_table)
    {
        $result = $this->con->query("SELECT * FROM `$this->banco`.`$in_table`");
        
        if($result->num_rows > 0)
        {
            while( $row = $result->fetch_object() )
            {
                $user_arr[] = $row;
            }
            return $user_arr;
        }
        else return false;
    }
    
    
    
    public function __destruct()
    {
        $this->con->close();
    }
}